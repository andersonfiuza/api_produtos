package br.com.lead.collector.models;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
//@Table(name = "leads")
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //@Column(name = "nome_completo")
    @NotNull(message = "Nome nao pode ser nulo")
    @NotBlank(message = "Nao pode pode ser branco")
    @Size(min =3, message = "Nome no minimo com 3 ")
    private String nome;
    //@Column(name = "CADASTRO_PESSOA_FISICA")
    @CPF(message = "CPF é invalido")
    private String cpf;
    @Email(message =  "E-mail é inválido")

    private String email;
    private String telefone;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Lead() {
    }
}
