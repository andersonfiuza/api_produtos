package br.com.lead.collector.DTOs;

public class RespostaDTO {

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public RespostaDTO(String resultado) {
        this.resultado = resultado;
    }

    private String resultado;

    public RespostaDTO() {
    }
}
