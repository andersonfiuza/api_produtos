package br.com.lead.collector.services;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.swing.text.html.Option;
import java.util.Optional;

@Service
public class ProdutoService {


    @Autowired
    private ProdutoRepository produtoRepository;


    public Produto salvaProduto(Produto produto) {

        Produto objproduto = produtoRepository.save(produto);
        return  objproduto;
    }

    public Iterable<Produto> lerTodosOsProdutos(){
        return produtoRepository.findAll();
    }

    public Produto buscarProduto(int id){
        Optional<Produto> leadOptional = produtoRepository.findById(id);

        if(leadOptional.isPresent()){
            Produto produto  = leadOptional.get();
            return produto;
        }else {
            throw new RuntimeException("O lead não foi encontrado");
        }
    }

    public Produto buscarProdutoPeloId(int id){
        Optional<Produto> leadOptional = produtoRepository.findById(id);

        if(leadOptional.isPresent()){
            Produto produto  = leadOptional.get();
            return produto;
        }else {
            throw new RuntimeException("O lead não foi encontrado");
        }
    }


    public Produto atualizarProduto(int id, Produto produto){
        Produto produtoDB = buscarProdutoPeloId(id);

        produto.setIdproduto(produtoDB.getIdproduto());
        return produtoRepository.save(produto);
    }

    public void deletarProduto(int id){
        if(produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);
        }else {
            throw new RuntimeException("Registro não existe");
        }
    }

}
